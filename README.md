Receipe Finder
==============

Given a list of items in the fridge (presented as a csv list), and a collection of recipes (a
collection of JSON formatted recipes), produce a recommendation for what to cook tonight.
Program should be written to take two inputs; fridge csv list, and the json recipe data. How you
choose to implement this is up to you; you can write a console application which takes input file
names as command line args, or as a web page which takes input through a form.
The only rule is that it must run and return a valid result using the provided input data.

For Test Data files, go to test/ directory:
--------------

    receipe.json
    fridge_items.csv

Notes:
● An ingredient that is past its use­by date cannot be used for cooking.
● If more than one recipe is found, then preference should be given to the recipe with the
closest use­by item
● If no recipe is found, the program should return “Order Takeout”
● Program should be all­inclusive and a run script included
● Please provide a complete copy of a git repository, or a link to a github/public repo
    ○ We want to be able to see your commit history
● Include unit tests

Using the sample input above, the program should return "salad sandwich".