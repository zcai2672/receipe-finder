<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 8/29/14
 * Time: 11:10 AM
 */
require ('ReceipeFinder.php');

class ReceipeFinderTest extends PHPUnit_Framework_TestCase {
    // Test Object creation
    public $testObj;
    public $fullObj;
    public $receipeFile;
    public $fridgeItemsFile;


    // Set up the test object instance
    public  function setUp(){
        $this->receipeFile =getcwd().'/test/receipe.json';
        $this->fridgeItemsFile =getcwd().'/test/fridge_items.csv';
        $this->testObj = new ReceipeFinder();
        $this->fullObj = new ReceipeFinder($this->receipeFile,  $this->fridgeItemsFile );

    }
    // Test if class exist for dependency testing
    public function testClassExist(){
        $this->assertClassHasAttribute('receipeArray', 'receipeFinder');
    }
    // Test loading test files
    public function testLoadReceipe(){
        $file = getcwd().'/test/receipe.json';
        $this->assertTrue($this->testObj->loadReceipe($this->receipeFile)); // return true when file is loaded
    }
    // Test loading test files
    public function testLoadFridgeItems(){
        $file = getcwd().'/test/fridge_items.csv';
        $this->assertTrue($this->testObj->loadFridgeItems($this->fridgeItemsFile )); // return true when file is loaded
    }
    // Test formatted fridge Items
    public function testFormatFridgeItems(){
        $file = getcwd().'/test/fridge_items.csv';
        $this->testObj->loadFridgeItems( $this->fridgeItemsFile );
        $this->assertEquals(10, $this->testObj->getFridgeItems()['cheese']['amount']);
    }
    // Find individual items in the fridge
    public function testFindFridgeItem(){
        $ingredient = json_decode('{"item":"bread","amount":"2","unit":"slices"}');
        $this->assertTrue($this->fullObj->findFridgeItem($ingredient));
    }

    // Find individual ingredient that doesn't exist in the fridge
    public function testFindFridgeItemDontExist(){
        $ingredient = json_decode('{"item":"fish","amount":"2","unit":"kilos"}');
        $this->assertFalse($this->fullObj->findFridgeItem($ingredient));
    }
    // Find individual items in the fridge that has greater amount then the item in the fridge
    public function testFindFridgeItemWrongAmount(){
        $ingredient = json_decode('{"item":"bread","amount":"100","unit":"slices"}');
        $this->assertFalse($this->fullObj->findFridgeItem($ingredient));
    }

    // validate if receipe is valid
    public function testValidateReceipe(){
          $receipe= json_decode('{
            "name":"grilled cheese on toast",
            "ingredients":[
                {"item":"bread","amount":"2","unit":"slices"},
                {"item":"cheese","amount":"2","unit":"slices"}
            ]
        }');
        $this->assertTrue($this->fullObj->validateReceipe($receipe));
    }

    // Mixed salad has expired in 26/12/2013 so should return false
    public function testValidateReceipeExpired(){
        $receipe= json_decode('{
        "name":"salad sandwich",
        "ingredients":[
            {"item":"bread","amount":"2","unit":"slices"},
            {"item":"mixed salad","amount":"200","unit":"grams"}
        ]}');

        $this->assertFalse($this->fullObj->validateReceipe($receipe));
    }

    // return false if ingredients doesn't exist
    public function testValidateReceipeDontExist(){
        $receipe= json_decode('{
            "name":"Beijing Roast Duck",
            "ingredients":[
                {"item":"mixed salad","amount":"20","unit":"grams"},
                {"item":"duck","amount":"2","unit":"kilo"},
                {"item":"honey source","amount":"20","unit":"ml"}
            ]
        }');
        $this->assertFalse($this->fullObj->validateReceipe($receipe));
    }

    //get the expiry date for a ingredient item
    public function testGetItemUseByDate(){
        $ingredientName = 'bread';
        $getDate = $this->fullObj->getItemUseByDate('bread');
        $this->assertEquals($getDate, 1419465600);
    }

    //get the expiry date for a ingredient item when item is not found
    public function testGetItemUseByDateFalse(){
        $getDate = $this->fullObj->getItemUseByDate('notfood');
        $this->assertEquals($getDate, -1);
    }

    public function testGetReceipeUseByDate(){
        $receipe= json_decode('{
            "name":"grilled cheese on toast with peanut butter ",
            "ingredients":[
                {"item":"bread","amount":"2","unit":"slices"},
                {"item":"cheese","amount":"2","unit":"slices"},
                {"item":"peanut butter","amount":"100","unit":"grams"}
            ]
        }');

         $peanutButterUseBy = strtotime('2-12-2014'); // date from data file
        //1417478400
        $this->assertEquals($this->fullObj->getReceipeUseByDate($receipe),$peanutButterUseBy);

    }

    // Test for the receipe that is most matched, it gets all the receipe and return the correct
    public function testFindMatchedReceipe(){
        $result="grilled cheese on toast";
        $this->assertEquals($this->fullObj->findMatchedReceipe(), $result);
    }

    // Test for the receipe that is most matched, it gets all the receipe and return the correct
    public function testFindMatchedReceipeNoMatch(){
        $result="Order Takeout";
        $file = getcwd().'/test/receipe_no_match.json';
        $this->fullObj->loadReceipe($file);
        $this->assertEquals($this->fullObj->findMatchedReceipe(), $result);
    }

}
 