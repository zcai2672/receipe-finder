

$(document).ready(function(){
    $( '#receipeFinderForm' )
        .submit( function( e ) {
            $.ajax( {
                url: 'processData.php',
                type: 'POST',
                data: new FormData( this ),
                processData: false,
                contentType: false
            }).done(function(msg){

               $("#results").html('<H2>'+msg+'<H2>');
            });
            e.preventDefault();
        } );

});