<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 8/29/14
 * Time: 11:09 AM
 */

class ReceipeFinder {

    private $receipeArray;
    private $fridgeItems;
    private $sortedFridgeItemsArray;
    private $today;
    private $resultReceipe;
     function  __construct($receipeFile= null, $fridgeItemsFile=null){

         $this->today = strtotime(date('d-m-Y'));
         if(!is_null($receipeFile)){
            $this->loadReceipe($receipeFile);
         };

         if(!is_null($fridgeItemsFile)){
            $this->loadFridgeItems($fridgeItemsFile);
         };
     }

    public function loadReceipe($file){
        $fileContent = file_get_contents($file);
        $this->receipeArray = json_decode($fileContent);
        if(sizeof($this->receipeArray)<1){
            return FALSE;
        }

        return TRUE;
    }

    public function loadFridgeItems($file){
        $fileContent = file($file);
        $this->fridgeItems = $fileContent;
        if(sizeof($this->fridgeItems)<1){
            return FALSE;
        }
        $this->formatFridgeItems();
        return TRUE;
    }
    //convert existing format into associative format
    public function formatFridgeItems(){
        foreach($this ->fridgeItems as $fridgeItem){
            $item = array(
                'amount' =>str_getcsv($fridgeItem)[1],
                'unit' =>str_getcsv($fridgeItem)[2],
                'useByDate' =>strtotime(str_replace('/','-', str_getcsv($fridgeItem)[3]))  // converting dd/mm/yyyy to dd-mm-yyyy for correct Australian date conversion
            );


            if($item['useByDate']>$this->today){  // if item not yet expired add to sorted list, throw away expired items
                $this->sortedFridgeItemsArray[str_getcsv($fridgeItem)[0]] =$item;
            };
        }
    }

    // Find Ingredient in the fridge
    public function findFridgeItem($ingredient){

        if(!isset($ingredient->item)){
            return FALSE;
        };

        if(isset($this->sortedFridgeItemsArray[$ingredient->item])){
            $fridgeItem = $this->sortedFridgeItemsArray[$ingredient->item];
            //if not enough in the fridge or the unit is wrong return false
            if($fridgeItem['amount'] < $ingredient->amount || $fridgeItem['unit'] != $ingredient->unit ){
                 return  FALSE;
            };
        }else{
            return FALSE;
        }

        return TRUE;
    }

    // validate the ingredients in the receipe if is still in the fridge
    public function validateReceipe($receipe){

        foreach($receipe->ingredients as $ingredient){
            if(!$this->findFridgeItem($ingredient)){ // if ingredient is not found return false immediately
                return FALSE;
            }
        }
        return TRUE;
    }
    // return use by date for item
    public function getItemUseByDate($itemName){
        if(isset($this->sortedFridgeItemsArray[$itemName])){
            return $this->sortedFridgeItemsArray[$itemName]['useByDate'];
        }else{
            return -1;
        }
    }
    public function getReceipeUseByDate($receipe){
        $earliestUseByDate = null;
        foreach($receipe->ingredients as $ingredient){
            if($earliestUseByDate == null)
            {
                $earliestUseByDate = $this->getItemUseByDate($ingredient->item);
            }

            elseif($earliestUseByDate > $this->getItemUseByDate($ingredient->item)){
                $earliestUseByDate = $this->getItemUseByDate($ingredient->item);
            }

        }
        return $earliestUseByDate;
    }
    public function findMatchedReceipe(){
        $earliestUseByDate=null;
        $resultReceipe=array();
        foreach($this->receipeArray as $receipe){
            if($this->validateReceipe($receipe)){
                if($earliestUseByDate == null)
                {
                    $earliestUseByDate = $this->getReceipeUseByDate($receipe);
                    $resultReceipe = $receipe;
                }

                elseif($earliestUseByDate > $this->getReceipeUseByDate($receipe)){
                    $earliestUseByDate = $this->getReceipeUseByDate($receipe);
                    $resultReceipe = $receipe;
                }
            }
        }
        if(sizeof($resultReceipe) == 0){
            return "Order Takeout";
        }else {
            $this->resultReceipe = $resultReceipe;
            return $resultReceipe->name;
        }
    }
    public function getFridgeItems(){
        return $this->sortedFridgeItemsArray;
    }

}
